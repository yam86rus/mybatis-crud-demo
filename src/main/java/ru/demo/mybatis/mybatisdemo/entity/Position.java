package ru.demo.mybatis.mybatisdemo.entity;


import java.time.LocalDate;

public class Position {
    private Mark mark;
    private Model model;
    private LocalDate create_date;
    private Integer mileage;
    private Double price;

    public Position() {
    }

    public Position(Mark mark, Model model, LocalDate create_date, Integer mileage, Double price) {
        this.mark = mark;
        this.model = model;
        this.create_date = create_date;
        this.mileage = mileage;
        this.price = price;
    }

    public Mark getMark() {
        return mark;
    }

    public void setMark(Mark mark) {
        this.mark = mark;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public LocalDate getCreate_date() {
        return create_date;
    }

    public void setCreate_date(LocalDate create_date) {
        this.create_date = create_date;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
