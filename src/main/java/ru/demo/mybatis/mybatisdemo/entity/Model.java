package ru.demo.mybatis.mybatisdemo.entity;

import java.time.LocalDate;

public class Model {
    private String name;

    private LocalDate begin_create;

    private LocalDate finish_create;

    public Model() {
    }

    public Model(String name, LocalDate begin_create, LocalDate finish_create) {
        this.name = name;
        this.begin_create = begin_create;
        this.finish_create = finish_create;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBegin_create() {
        return begin_create;
    }

    public void setBegin_create(LocalDate begin_create) {
        this.begin_create = begin_create;
    }

    public LocalDate getFinish_create() {
        return finish_create;
    }

    public void setFinish_create(LocalDate finish_create) {
        this.finish_create = finish_create;
    }
}
