package ru.demo.mybatis.mybatisdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.demo.mybatis.mybatisdemo.entity.Mark;
import ru.demo.mybatis.mybatisdemo.mapper.MarkMapper;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class MarkService {
    private final MarkMapper markMapper;

    @Autowired
    public MarkService(MarkMapper markMapper) {
        this.markMapper = markMapper;
    }

    public List<Mark> getMarks() {
        return markMapper.findAll();
    }

    @Transactional
    public Integer createNewMark(Mark mark) {
        return markMapper.saveMark(mark);
    }

    public Mark getMarkById(Integer id) {
        return markMapper.findById(id);
    }

    @Transactional
    public void updateMark(Mark mark) {
        markMapper.saveMark(mark);
    }

    @Transactional
    public void deleteMark(Integer id) {
        markMapper.deleteMark(id);
    }
}
