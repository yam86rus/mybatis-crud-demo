package ru.demo.mybatis.mybatisdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.demo.mybatis.mybatisdemo.entity.Model;
import ru.demo.mybatis.mybatisdemo.mapper.ModelMapper;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ModelService {

    private final ModelMapper modelMapper;

    @Autowired
    public ModelService(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    public List<Model> getModels() {
        return modelMapper.findAll();
    }

    @Transactional
    public Integer createNewModel(Model model) {
        return modelMapper.saveModel(model);
    }

    public Model getModelById(Integer id) {
        return modelMapper.findById(id);
    }

    @Transactional
    void updateModel(Model model) {
        modelMapper.update(model);
    }

    @Transactional
    public void deleteModel(Integer id) {
        modelMapper.deleteModel(id);
    }
}
