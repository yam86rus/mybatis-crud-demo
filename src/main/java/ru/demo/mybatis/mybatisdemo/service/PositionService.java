package ru.demo.mybatis.mybatisdemo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.demo.mybatis.mybatisdemo.dto.AddPositionRequest;
import ru.demo.mybatis.mybatisdemo.dto.GetPositionResponse;
import ru.demo.mybatis.mybatisdemo.mapper.PositionMapper;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class PositionService {
    private final PositionMapper positionMapper;

    @Autowired
    public PositionService(PositionMapper positionMapper) {
        this.positionMapper = positionMapper;
    }

    public List<GetPositionResponse> getPositions() {
        return positionMapper.findAll();
    }

    @Transactional
    public Integer createPosition(AddPositionRequest positionDTORequest) {
        return positionMapper.savePosition(positionDTORequest);
    }
    public List<GetPositionResponse> getByModelName(String modelName) {
        return positionMapper.findByModelName(modelName);
    }

    public List<GetPositionResponse> getByMarkName(String markName) {
        return positionMapper.findByMarkName(markName);
    }

    public List<GetPositionResponse> getByPrice(Double price) {
        return positionMapper.findByPrice(price);
    }

    @Transactional
    public void deletePosition(Integer id) {
        positionMapper.deletePosition(id);
    }


}
