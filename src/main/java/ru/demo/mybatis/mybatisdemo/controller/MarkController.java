package ru.demo.mybatis.mybatisdemo.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.demo.mybatis.mybatisdemo.entity.Mark;
import ru.demo.mybatis.mybatisdemo.service.MarkService;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@Tag(name = "Таблица \"Марка\"")
public class MarkController {
    private final MarkService markService;

    @Autowired
    public MarkController(MarkService markService) {
        this.markService = markService;
    }

    @GetMapping("/mark")
    @Operation(summary = "Получить все записи")
    public List<Mark> getAll() {
        return markService.getMarks();
    }

    @PostMapping("/mark")
    @Operation(summary = "Добавить новую запись")
    public ResponseEntity<Integer> createMark(@RequestBody Mark mark) {
        Integer result = markService.createNewMark(mark);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @GetMapping("/mark/{id}")
    @Operation(summary = "Получить запись по id")
    public ResponseEntity<Mark> update(@PathVariable Integer id) {
        Mark mark = markService.getMarkById(id);
        return new ResponseEntity<>(mark, HttpStatus.OK);
    }

    @DeleteMapping("/mark/{id}")
    @Operation(summary = "Удалить запись по id")
    public ResponseEntity<?> deleteMark(@PathVariable Integer id) {
        markService.deleteMark(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
