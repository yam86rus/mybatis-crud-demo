package ru.demo.mybatis.mybatisdemo.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.demo.mybatis.mybatisdemo.dto.AddPositionRequest;
import ru.demo.mybatis.mybatisdemo.dto.GetPositionResponse;
import ru.demo.mybatis.mybatisdemo.service.PositionService;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@Tag(name = "Таблица \"Позиция в магазине\"")
public class PositionController {
    private final PositionService positionService;

    @Autowired
    public PositionController(PositionService positionService) {
        this.positionService = positionService;
    }

    @GetMapping("/position")
    @Operation(summary = "Получить все записи")
    public List<GetPositionResponse> getAll() {
        return positionService.getPositions();
    }

    @GetMapping("/position/model/{modelName}")
    @Operation(summary = "Получить записи по названию модели")
    public List<GetPositionResponse> getByModelName(@PathVariable String modelName) {
        return positionService.getByModelName(modelName);
    }

    @GetMapping("/position/mark/{markName}")
    @Operation(summary = "Получить записи по названию марки")
    public List<GetPositionResponse> getByMarkName(@PathVariable String markName) {
        return positionService.getByMarkName(markName);
    }

    @GetMapping("/position/price/{price}")
    @Operation(summary = "Получить записи по цене <=")
    public List<GetPositionResponse> getByMarkName(@PathVariable Double price) {
        return positionService.getByPrice(price);
    }

    @PostMapping("/position")
    @Operation(summary = "Добавить новую запись")
    public ResponseEntity<Integer> createMark(@RequestBody AddPositionRequest positionDTORequest) {
        Integer result = positionService.createPosition(positionDTORequest);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @DeleteMapping("/position/{id}")
    @Operation(summary = "Удалить запись по id")
    public ResponseEntity<?> deleteMark(@PathVariable Integer id) {
        positionService.deletePosition(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
