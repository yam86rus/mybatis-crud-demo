package ru.demo.mybatis.mybatisdemo.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.demo.mybatis.mybatisdemo.entity.Model;
import ru.demo.mybatis.mybatisdemo.service.ModelService;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@Tag(name = "Таблица \"Модель\"")
public class ModelController {

    private final ModelService modelService;

    @Autowired
    public ModelController(ModelService modelService) {
        this.modelService = modelService;
    }

    @GetMapping("/model")
    @Operation(summary = "Получить все записи")
    public List<Model> getAll() {
        return modelService.getModels();
    }

    @PostMapping("/model")
    @Operation(summary = "Добавить новую запись")
    public ResponseEntity<Integer> createMark(@RequestBody Model model) {
        Integer result = modelService.createNewModel(model);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @GetMapping("/model/{id}")
    @Operation(summary = "Получить запись по id")
    public ResponseEntity<Model> update(@PathVariable Integer id) {
        Model model = modelService.getModelById(id);
        return new ResponseEntity<>(model, HttpStatus.OK);
    }

    @DeleteMapping("/model/{id}")
    @Operation(summary = "Удалить запись по id")
    public ResponseEntity<?> deleteMark(@PathVariable Integer id) {
        modelService.deleteModel(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
