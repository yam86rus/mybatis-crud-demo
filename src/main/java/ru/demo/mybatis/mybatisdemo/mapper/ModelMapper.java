package ru.demo.mybatis.mybatisdemo.mapper;

import org.apache.ibatis.annotations.*;
import ru.demo.mybatis.mybatisdemo.entity.Model;

import java.util.List;

@Mapper
public interface ModelMapper {
    @Select("select * from model")
    List<Model> findAll();

    @Insert("insert into model (name, begin_create,finish_create) values (#{name}, #{begin_create}, #{finish_create})")
    Integer saveModel(Model model);

    @Update("Update model set name=#{name} begin_create=#{begin_create} finish_create=#{finish_create} where id = #{id}")
    void update(Model model);

    @Delete("Delete from model where id=#{id}")
    void deleteModel(Integer id);

    @Select("select * from model where id = #{id}")
    Model findById(Integer id);
}
