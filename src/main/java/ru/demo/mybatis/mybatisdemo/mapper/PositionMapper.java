package ru.demo.mybatis.mybatisdemo.mapper;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import ru.demo.mybatis.mybatisdemo.dto.AddPositionRequest;
import ru.demo.mybatis.mybatisdemo.dto.GetPositionResponse;

import java.util.List;

@Mapper
public interface PositionMapper {
    @Select("select m.name               as mark,\n" +
            "       m2.name              as model,\n" +
            "       position.mileage     as mileage,\n" +
            "       position.price       as price,\n" +
            "       position.create_date as create_date\n" +
            "from position\n" +
            "         join mark m on m.id = position.mark\n" +
            "         join model m2 on m2.id = position.model")
    List<GetPositionResponse> findAll();

    @Select("select m.name               as mark,\n" +
            "       m2.name              as model,\n" +
            "       position.mileage     as mileage,\n" +
            "       position.price       as price,\n" +
            "       position.create_date as create_date\n" +
            "from position\n" +
            "         join mark m on m.id = position.mark\n" +
            "         join model m2 on m2.id = position.model\n" +
            "where m2.name =#{modelName}")
    List<GetPositionResponse> findByModelName(String modelName);

    @Select("select m.name               as mark,\n" +
            "       m2.name              as model,\n" +
            "       position.mileage     as mileage,\n" +
            "       position.price       as price,\n" +
            "       position.create_date as create_date\n" +
            "from position\n" +
            "         join mark m on m.id = position.mark\n" +
            "         join model m2 on m2.id = position.model\n" +
            "where m.name =#{markName}")
    List<GetPositionResponse> findByMarkName(String markName);

    @Select("select m.name               as mark,\n" +
            "       m2.name              as model,\n" +
            "       position.mileage     as mileage,\n" +
            "       position.price       as price,\n" +
            "       position.create_date as create_date\n" +
            "from position\n" +
            "         join mark m on m.id = position.mark\n" +
            "         join model m2 on m2.id = position.model\n" +
            "where position.price <= #{price}")
    List<GetPositionResponse> findByPrice(Double price);

    @Insert("insert into position (mark, model,create_date,mileage,price) values (#{mark}, #{model}, #{create_date}, #{mileage}, #{price})")
    Integer savePosition(AddPositionRequest positionDTORequest);

    @Delete("Delete from position where id=#{id}")
    void deletePosition(Integer id);
}

