package ru.demo.mybatis.mybatisdemo.mapper;

import org.apache.ibatis.annotations.*;
import ru.demo.mybatis.mybatisdemo.entity.Mark;

import java.util.List;

@Mapper
public interface MarkMapper {
    @Select("select * from mark")
    List<Mark> findAll();

    @Insert("insert into mark (name, country) values (#{name}, #{country})")
    Integer saveMark(Mark mark);

    @Update("Update mark set name=#{name} country=#{country} where id = #{id}")
    void update(Mark mark);

    @Delete("Delete from mark where id=#{id}")
    public void deleteMark(Integer id);

    @Select("select * from mark where id = #{id}")
    Mark findById(Integer id);
}
